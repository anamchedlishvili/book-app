export const fetchBooks = () => {
  const API_KEY = "AIzaSyC4uLXmU4W7L4GWHwPDwdxAlebyRmlHKP8";
  return dispatch => {
    dispatch({ type: "FETCH_BOOKS_START" });
    fetch(
      `https://www.googleapis.com/books/v1/volumes?q=wind+inauthor:rothfuss&key=${API_KEY}`
    )
      .then(response => response.json())
      .then(data => {
        dispatch({ type: "FETCH_BOOKS_DONE", books: data });
      })
      .catch(err => {
        dispatch({ type: "FETCH_BOOKS_ERROR", error: err });
      });
  };
};

export const toggleFavorite = (id, _favorites, _books) => {
  let newFavorites = [..._favorites];
  let books = _books;
  let inFavs = newFavorites.find(book => book.id === id);
  if (inFavs) {
    newFavorites.splice(newFavorites.indexOf(inFavs), 1);
  } else {
    let newFavBook = books.items.find(book => book.id === id);
    newFavorites.push(newFavBook);
  }
  return {
    type: "TOGGLE_FAVORITE",
    payload: newFavorites
  };
};
