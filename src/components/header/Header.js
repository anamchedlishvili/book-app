import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import "./header.css";

class Header extends Component {
  render() {
    return (
      <header className="page-header">
        <nav className="page-header__navigation">
          <NavLink to="/" exact className="page-header__navigation-link">
            Home
          </NavLink>
          <NavLink to="/favorites" className="page-header__navigation-link">
            Favorites
          </NavLink>
        </nav>
      </header>
    );
  }
}

export default Header;
