import React, { Component } from 'react';
import { connect } from 'react-redux';
import { toggleFavorite } from '../../actions/bookActions';
import BookListItem from '../booklistItem/BookListItem';

class Favorites extends Component {
  handleToggle = id => {
    this.props.toggleFavorite(id, this.props.favorites, this.props.books);
  };
  render() {
    return (
      <section className="wrapper">
        <BookListItem
          books={this.props.favorites}
          favorites={this.props.favorites}
          handleToggle={this.handleToggle}
        />
      </section>
    );
  }
}

const mapStateToProps = state => {
  return {
    favorites: state.favorites,
    books: state.books
  };
};

const mapDispatchToProps = dispatch => {
  return {
    toggleFavorite: (id, _favorites, _books) => {
      dispatch(toggleFavorite(id, _favorites, _books));
    }
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Favorites);
