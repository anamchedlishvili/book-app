import React from 'react';
import { Link } from 'react-router-dom';

const BookListItem = props => {
  let contains = id => props.favorites.find(item => item.id === id);
  return (
    <ul className="book-list">
      {props.books &&
        props.books.map(book => {
          return (
            <li className="book-list__item" key={book.id}>
              <div className="book-list__item__img-container">
                <img
                  src={book.volumeInfo.imageLinks.thumbnail}
                  alt={book.volumeInfo.title}
                  className="book-list__item__img"
                />
              </div>
              <div className="book-list__item__info">
                <h2 className="book-list__item__info__title">
                  {book.volumeInfo.title}
                </h2>
                <p className="book-list__item__info__description">
                  {book.volumeInfo.description}
                </p>
                <div className="book-list__item__info__btns">
                  <button
                    className={
                      contains(book.id) ? 'add-btn add-btn--active' : 'add-btn'
                    }
                    onClick={() => props.handleToggle(book.id)}
                  >
                    {contains(book.id)
                      ? 'Remove from Favorites'
                      : 'Add to Favorites'}
                  </button>
                  <Link to={`/details/${book.id}`} className="details-link">
                    See more
                  </Link>
                </div>
              </div>
            </li>
          );
        })}
    </ul>
  );
};

export default BookListItem;
