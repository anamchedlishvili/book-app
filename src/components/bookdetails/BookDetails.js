import React, { Component } from 'react';
import './bookdetails.css';
import { connect } from 'react-redux';
import { toggleFavorite } from '../../actions/bookActions';

class BookDetails extends Component {
  handleToggle = () => {
    this.props.toggleFavorite(
      this.props.book.id,
      this.props.favorites,
      this.props.books
    );
  };
  render() {
    console.log('detail props', this.props);
    const book = this.props.book ? (
      <div className="book-details">
        <div className="book-details__img-container">
          <img
            src={this.props.book.volumeInfo.imageLinks.thumbnail}
            alt={this.props.book.volumeInfo.title}
            className="book-details__img"
          />
        </div>
        <div className="book-details__info">
          <h2 className="book-details__info__title">
            {this.props.book.volumeInfo.title}
          </h2>
          <span className="book-details__info__author">
            {this.props.book.volumeInfo.authors[0]}
          </span>
          <p className="book-details__info__description">
            {this.props.book.volumeInfo.description}
          </p>
          <div className="book-details__info__edition">
            <span className="book-details__info__date info-span">
              Published: {this.props.book.volumeInfo.publishedDate}
            </span>
            <span className="book-details__info__pages info-span">
              {this.props.book.volumeInfo.pageCount} pages
            </span>
          </div>
          <div className="book-list__item__info__btns">
            <button
              className={
                this.props.favorite ? 'add-btn add-btn--active' : 'add-btn'
              }
              onClick={this.handleToggle}
            >
              {this.props.favorite
                ? 'Remove from Favorites'
                : 'Add to Favorites'}
            </button>
          </div>
        </div>
      </div>
    ) : (
      <div>No book found</div>
    );
    return <section className="wrapper">{book}</section>;
  }
}

const mapStateToProps = (state, ownProps) => {
  let bookId = ownProps.match.params.bookId;
  console.log('state in details', state);
  return {
    book: state.books.items.find(book => book.id === bookId),
    favorite: state.favorites.find(book => book.id === bookId),
    books: state.books,
    favorites: state.favorites
  };
};

const mapDispatchToProps = dispatch => {
  return {
    toggleFavorite: (id, _favorites, _books) => {
      dispatch(toggleFavorite(id, _favorites, _books));
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BookDetails);
