import React, { Component } from 'react';
import './booklist.css';
import { connect } from 'react-redux';
import { toggleFavorite, fetchBooks } from '../../actions/bookActions';
import BookListItem from '../booklistItem/BookListItem';

class BookList extends Component {
  componentDidMount() {
    this.props.fetchBooks();
    console.log('in component', this.props.books);
  }
  handleToggle = id => {
    this.props.toggleFavorite(id, this.props.favorites, this.props.books);
  };
  render() {
    return (
      <main className="wrapper">
        <BookListItem
          books={this.props.books.items}
          favorites={this.props.favorites}
          handleToggle={this.handleToggle}
        />
      </main>
    );
  }
}

const mapStateToProps = state => {
  return {
    books: state.books,
    favorites: state.favorites
  };
};

const mapDispatchToProps = dispatch => {
  return {
    toggleFavorite: (id, _favorites, _books) => {
      dispatch(toggleFavorite(id, _favorites, _books));
    },
    fetchBooks: () => {
      dispatch(fetchBooks());
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BookList);
