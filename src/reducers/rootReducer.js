const initState = {
  loadingBooks: false,
  error: null,
  books: [],
  favorites: []
};

const rootReducer = (state = initState, action) => {
  // console.log(action);
  if (action.type === 'TOGGLE_FAVORITE') {
    return {
      ...state,
      favorites: action.payload
    };
  }
  if (action.type === 'FETCH_BOOKS_START') {
    return {
      ...state,
      loadingBooks: true
    };
  }
  if (action.type === 'FETCH_BOOKS_ERROR') {
    return {
      ...state,
      loadingBooks: false,
      error: action.error
    };
  }
  if (action.type === 'FETCH_BOOKS_DONE') {
    return {
      ...state,
      loadingBooks: false,
      error: false,
      books: action.books
    };
  }
  return state;
};

export default rootReducer;
