import React from "react";
import "./App.css";
import { Route } from "react-router-dom";
import Header from "./components/header/Header";
import BookList from "./components/booklist/BookList";
import BookDetails from "./components/bookdetails/BookDetails";
import Favorites from "./components/favorites/Favorites";

function App() {
  return (
    <React.Fragment>
      <Header />
      <Route path="/" exact component={BookList} />
      <Route path="/favorites" component={Favorites} />
      <Route path="/details/:bookId" component={BookDetails} />
    </React.Fragment>
  );
}

export default App;
